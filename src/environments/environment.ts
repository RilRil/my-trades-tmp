// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    automaticDataCollectionEnabled: true,
    apiKey: "AIzaSyAs2ijXklkksKHdZlGHegJWbiIAqfkpA8A",
    authDomain: "trade-tools-test.firebaseapp.com",
    databaseURL: "https://trade-tools-test.firebaseio.com",
    projectId: "trade-tools-test",
    storageBucket: "trade-tools-test.appspot.com",
    messagingSenderId: "328567600063"
  }
};
