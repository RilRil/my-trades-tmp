import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';

@Injectable()
export class AuthGuardService implements CanActivate {
    constructor(public router: Router, public afAuth: AngularFireAuth) {}

    canActivate(): Observable<boolean> {
        return this.afAuth.authState.pipe(
            take(1),
            map(authState => !!authState),
            tap(authenticated => {
                if (!authenticated) {
                    this.router.navigate(['/login']);
                    return true;
                }
                return false;
            })
        );
    }
}
