import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../../shared.module';
import {PositionInputDialogComponent} from './positionInputDialog.component';
import {MarketService} from '../market/market.service';

@NgModule({
    declarations: [PositionInputDialogComponent],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
    ],
    providers: [MarketService],
    entryComponents: [PositionInputDialogComponent]
})
export class PositionInputDialogModule {
}
