import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {IntTrade} from '../trade/trade.service';

@Component({
    selector: 'position-input-dialog',
    templateUrl: './positionInputDialog.component.html'
})
export class PositionInputDialogComponent {

    constructor(
        public dialog: MatDialog,
        @Inject(MAT_DIALOG_DATA) public data: {trade: IntTrade, value: number; quantity: number}
    ) {
    }

    cancel(): void {
        this.dialog.closeAll();
    }
}