import { Injectable } from '@angular/core'
import { AngularFireAuth } from '@angular/fire/auth'
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore'

import * as firebase from 'firebase/app'
import { Router } from '@angular/router'

@Injectable()
export class AuthService {
    private _user: firebase.User = null

    constructor(public afAuth: AngularFireAuth, public router: Router, private db: AngularFirestore) {
        afAuth.authState.subscribe(user => (this._user = user))

    }

    get user() {
        return this._user
    }

    get authenticated(): boolean {
        return this._user !== null
    }

    get id(): string {
        return this.authenticated ? this.user.uid : ''
    }

    createNewUser(userData) {
        let uid = userData.user.uid
        this.db
            .doc(`users/${uid}`)
            .ref.get()
            .then(doc => {
                if (!doc.exists) {
                    this.db
                        .collection(`users`)
                        .doc(uid)
                        .set({
                            profile: userData.additionalUserInfo.profile
                        })
                }
            })
    }

    loginWithGoogle() {
        this.afAuth.auth
            .signInWithPopup(new firebase.auth.GoogleAuthProvider())
            .then(data => {
                this.createNewUser(data)
                this.router.navigate(['trades'])
            })
            .catch(e => {
                console.log('authentication error', e)
            })
    }
    loginWithTwitter() {
        this.afAuth.auth
            .signInWithPopup(new firebase.auth.TwitterAuthProvider())
            .then(data => {
                this.createNewUser(data)
                this.router.navigate(['trades'])
            })
            .catch(e => {
                console.log('authentication error', e)
            })
    }

    signOut(): void {
        this.afAuth.auth.signOut().then(() => {
            this.router.navigate(['login'])
        })
    }
}
