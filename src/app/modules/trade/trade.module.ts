import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {PositionFormModule} from '../positionForm/positionForm.module';
import {TradeComponent} from './trade.component';
import {TradeService} from './trade.service';
import {SharedModule} from '../../shared.module';
import {PriceInputDialogModule} from '../priceInputDialog/priceInputDialog.module';

@NgModule({
    declarations: [TradeComponent],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        PositionFormModule,
        PriceInputDialogModule
    ],
    exports: [TradeComponent],
    providers: [TradeService]
})
export class TradeModule {
}
