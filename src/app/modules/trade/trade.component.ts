import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { TradeService, IntTrade, IntTradePosition, IntTradeData } from './trade.service';
import { MarketService } from '../market/market.service';
import { TimerObservable } from 'rxjs/observable/TimerObservable';

import { MatDialog } from '@angular/material';
import { PriceInputDialogComponent } from '../priceInputDialog/priceInputDialog.component';

@Component({
    selector: 'trade-component',
    templateUrl: './trade.component.html',
    styleUrls: ['./trade.style.less']
})
export class TradeComponent implements OnInit, OnDestroy {
    @Input() trade: IntTrade;
    @Output() updateTotals = new EventEmitter();

    tradeData: IntTradeData;
    pair: string;
    latestPriceForTradePair: number = 0;
    latestDiffPrice: number = 0;
    profit: number = 0;
    profitPercentage: number = 0;

    timerSubscription = null;
    editedPrice: number;
    editedPosition: number;
    isEditingPrice = false;
    positionsDisplayed = false;

    constructor(private tradeService: TradeService, private marketService: MarketService, public dialog: MatDialog) {}

    ngOnInit() {
        this.tradeData = this.tradeService.getTradeData(this.trade);
        this.pair = this.trade.symbol1 + '-' + this.trade.symbol2;

        if (this.trade.exchange !== 'none') {
            let timer = TimerObservable.create(0, 15000);
            this.timerSubscription = timer.subscribe(() => {
                this.showMarketLatestPrice();
            });
        }
    }

    ngOnDestroy(): void {
        if (this.timerSubscription) {
            this.timerSubscription.unsubscribe();
        }
    }

    showMarketLatestPrice() {
        this.marketService.getLatestPrice(this.trade.symbol1, this.trade.symbol2, this.trade.exchange).subscribe(res => {
            this.latestPriceForTradePair = this.marketService.round(res['data'], this.trade.symbol1);
            this.latestDiffPrice =
                this.tradeData.averagePrice <= 0
                    ? 0
                    : this.marketService.round(
                          this.latestPriceForTradePair - this.tradeData.averagePrice,
                          this.trade.symbol2
                      );
            this.updateProfitLossValues();
            this.emitTotals();
        });
    }

    updateProfitLossValues() {
        this.profit = this.getProfit();
        this.profitPercentage = this.getProfitPercentage();
    }

    cancelTrade() {
        let cancel = confirm('Are you sure?');
        if (cancel === true) {
            this.tradeService.cancelTrade(this.trade);
            this.emitTotals(true);
        }
    }

    closeTrade() {
        const dialogRef = this.dialog.open(PriceInputDialogComponent, {
            data: {
                priceSold: this.latestPriceForTradePair,
                amountAvailable: this.tradeData.amount,
                trade: this.trade
            }
        });

        dialogRef.afterClosed().subscribe(data => {
            if (data) {
                if (data.quantitySold && data.priceSold) {
                    // partial sell
                    this.tradeService.closeTradePartial(this.trade, data.priceSold, data.quantitySold, this.tradeData.averagePrice);
                } else if (data.priceSold && !data.quantitySold) {
                    this.tradeService.closeTrade(this.trade, data.priceSold, this.tradeData.amount);
                }
                this.emitTotals(true);
            }
            // add new ratio to stats
        });
    }

    emitTotals(reset?: boolean) {
        if (reset) {
            this.updateTotals.emit({
                reset: true,
                id: this.trade.id,
                totalProfit: 0,
                totalEngaged: 0
            });
        } else {
            this.updateTotals.emit({
                id: this.trade.id,
                totalProfit: this.getProfit(),
                totalEngaged: this.tradeData.value
            });
        }
    }

    getProfit(): number {
        return this.tradeService.getProfit(
            this.isEditingPrice ? this.editedPrice : this.latestPriceForTradePair,
            this.tradeData.averagePrice,
            this.tradeData.amount,
            this.trade.symbol2
        );
    }

    getProfitPosition(position): number {
        return this.tradeService.getProfit(
            this.latestPriceForTradePair,
            position.value,
            position.quantity,
            this.trade.symbol2
        );
    }

    getProfitPercentage(myPrice?: number): number {
        return this.tradeService.getPercentageProfit(
            this.isEditingPrice ? this.editedPrice : this.latestPriceForTradePair,
            myPrice ? myPrice : this.tradeData.averagePrice
        );
    }

    getValuePosition(position): number {
        return this.marketService.round(position.quantity * position.value, this.trade.symbol2);
    }

    submitPosition(position) {
        this.tradeService.submitPosition(position, this.trade);
    }

    editPrice() {
        this.isEditingPrice = !this.isEditingPrice;
        this.editedPrice = this.latestPriceForTradePair;
        this.updateProfitLossValues();
    }

    editPosition(index: number) {
        if (this.editedPosition === index) {
            this.editedPosition = null;
        } else {
            this.editedPosition = index;
        }
    }

    showPositions() {
        this.positionsDisplayed = !this.positionsDisplayed;
    }

    savePosition() {
        this.tradeService.savePositions(this.trade);
    }

    deletePosition(index: number) {
        this.trade.positions.splice(index, 1);
        this.tradeService.savePositions(this.trade);
    }
}
