import { Injectable } from '@angular/core'
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore'
import { AngularFireAuth } from '@angular/fire/auth'
import { Observable, of } from 'rxjs'
import { map } from 'rxjs/operators'
import { MarketService } from '../market/market.service'
import { AuthService } from '../auth/auth.service'

export interface IntTradePosition {
    quantity: number
    value: number
    type?: string
}

export interface IntTrade {
    id?: string // firestore id
    user: string
    symbol1: string
    symbol2: string
    positions: IntTradePosition[]
    canceled: boolean
    open: boolean
    priceClose: number
    dateOpen: number
    dateClose: number
    exchange: string
}

export interface IntTradeData {
    amount: number
    value: number
    averagePrice: number
}

@Injectable()
export class TradeService {
    user = null
    private tradesCollec: AngularFirestoreCollection<{}>
    private trades: Observable<any[]>

    private closedTrades: Observable<any[]>

    constructor(private marketService: MarketService, private db: AngularFirestore, private authService: AuthService) {
        this.tradesCollec = this.db.doc(`users/${this.authService.id}`).collection('trades', ref =>
            ref
                .where('open', '==', true)
                .where('canceled', '==', false)
                .where('user', '==', this.authService.id)
                .orderBy('dateOpen', 'desc')
        )
        this.trades = this.tradesCollec.snapshotChanges().pipe(
            map(actions =>
                actions.map(a => {
                    const data = a.payload.doc.data()
                    const id = a.payload.doc.id
                    return { id, ...data }
                })
            )
        )
    }

    getTrades() {
        return this.trades
    }

    getHistoryTrades() {
        let collec = this.db.doc(`users/${this.authService.id}`).collection('trades', ref =>
            ref
                .where('open', '==', false)
                .where('canceled', '==', false)
                .where('user', '==', this.authService.id)
                .orderBy('dateClose', 'desc')
        )
        return collec.snapshotChanges().pipe(
            map(actions =>
                actions.map(a => {
                    const data = a.payload.doc.data()
                    const id = a.payload.doc.id
                    return { id, ...data }
                })
            )
        )
    }

    closeTrade(trade: IntTrade, price: number, amount: number) {
        const date = new Date()
        const avgPrice = this.getAveragePrice(trade)
        this.tradesCollec.doc(trade.id).update({
            open: false,
            priceClose: price,
            dateClose: date.getTime(),
            priceAvg: avgPrice,
            profitPct: this.getPercentageProfit(price, avgPrice),
            profit: this.getProfit(price, avgPrice, amount, trade.symbol1)
        })
    }

    closeTradePartial(trade: IntTrade, price: number, quantity: number, averagePrice: number) {
        const date = new Date()
        let tradeCopy = { ...trade }
        tradeCopy.positions = [{ quantity: quantity, value: averagePrice }]
        tradeCopy.open = false
        tradeCopy.dateClose = date.getTime()
        tradeCopy.priceClose = price
        this.tradesCollec.add(tradeCopy)

        let positions = trade.positions
        positions.push({ quantity: quantity, value: price, type: 'sell' })
        this.tradesCollec.doc(trade.id).update({
            positions: positions
        })
    }

    cancelTrade(trade: IntTrade) {
        this.tradesCollec.doc(trade.id).update({
            canceled: true
        })
    }

    saveNewTrade(trade: IntTrade) {
        const date = new Date()
        trade.symbol1 = trade.symbol1.toUpperCase()
        trade.symbol2 = trade.symbol2.toUpperCase()
        trade.dateOpen = date.getTime()
        trade.user = this.authService.id
        return this.tradesCollec.add(trade)
    }

    savePositions(trade: IntTrade) {
        this.tradesCollec.doc(trade.id).update({
            positions: trade.positions
        })
    }

    submitPosition(position: IntTradePosition, toTrade) {
        toTrade.positions.push(position)
        this.tradesCollec.doc(toTrade.id).update({
            positions: toTrade.positions
        })
    }

    getTradeData(fromTrade: IntTrade): IntTradeData {
        let amount: number = this.getTotalAmount(fromTrade)
        let avg: number = this.marketService.round(this.getAveragePrice(fromTrade), fromTrade.symbol1)
        let averagePrice: number = amount > 0 ? avg : 0
        let value: number = this.marketService.round(this.getTotalValue(amount, averagePrice), fromTrade.symbol2)
        return { amount, averagePrice, value }
    }

    getTotalAmount(fromTrade: IntTrade): number {
        let total = 0
        if (!fromTrade.positions || !fromTrade.positions.length) {
            return total
        }
        fromTrade.positions.forEach(p => (total += p.quantity * (p.type && p.type === 'sell' ? -1 : 1)))

        return this.marketService.round(total, fromTrade.symbol1)
    }

    getTotalValue(amount: number, averagePrice: number): number {
        return amount * averagePrice
    }

    getAveragePrice(fromTrade: IntTrade): number {
        let average = 0
        let totalValue = 0
        let totalAmount = 0

        if (!fromTrade.positions || !fromTrade.positions.length) {
            return average
        }

        fromTrade.positions.forEach(p => {
            if (this.isPositionTypeBuy(p)) {
                totalAmount += p.quantity
                totalValue += p.quantity * p.value
            }
        })
        average = totalValue / totalAmount

        return average
    }

    getPercentageProfit(currentPrice, averagePriceTrade) {
        const p = averagePriceTrade > 0 ? ((currentPrice - averagePriceTrade) / averagePriceTrade) * 100 : 0
        return this.marketService.round(p)
    }

    getProfit(currentPrice: number, averagePrice: number, amount: number, currency: string) {
        return this.marketService.round((currentPrice - averagePrice) * amount, currency)
    }

    isPositionTypeBuy(pos: IntTradePosition) {
        return !pos.type || pos.type === 'buy'
    }
}
