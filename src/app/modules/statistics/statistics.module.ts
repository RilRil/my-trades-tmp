import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { SharedModule } from '../../shared.module'
import { StatisticsService } from './statistics.service'

@NgModule({
    declarations: [],
    imports: [CommonModule, SharedModule],
    exports: [],
    providers: [StatisticsService]
})
export class StatisticsModule {}
