import { Injectable } from '@angular/core'
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore'
import { Observable, of } from 'rxjs'
import { map } from 'rxjs/operators'
import { MarketService } from '../market/market.service'
import { AuthService } from '../auth/auth.service'

@Injectable()
export class StatisticsService {
    user = null
    statsDoc: AngularFirestoreDocument<{}>
    statsRatioCollec: AngularFirestoreCollection<{}>
    statistics: Observable<any>
    ratiosWinLoss: Observable<any[]>

    constructor(private marketService: MarketService, private db: AngularFirestore, private authService: AuthService) {
        // console.log('Cyril -- here statistics service', authService.id)
        this.statsDoc = this.db.doc(`statistics/${authService.id}`)
        this.statistics = this.statsDoc.valueChanges()
        this.statsRatioCollec = this.statsDoc.collection('ratiosWinLoss')
        this.ratiosWinLoss = this.statsRatioCollec.snapshotChanges()
    }

    getRatios() {
        return this.ratiosWinLoss
    }
}
