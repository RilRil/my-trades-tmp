import {NgModule} from '@angular/core';
import {SafeHtmlPipe} from './safeHTML.pipe';

@NgModule({
    declarations: [SafeHtmlPipe],
    exports: [SafeHtmlPipe]
})
export class PipesModule {
}
