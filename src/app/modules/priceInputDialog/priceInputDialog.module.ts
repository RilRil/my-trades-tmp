import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../../shared.module';
import {PriceInputDialogComponent} from './priceInputDialog.component';

@NgModule({
    declarations: [PriceInputDialogComponent],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
    ],
    exports: [],
    entryComponents: [PriceInputDialogComponent]
})
export class PriceInputDialogModule {
}
