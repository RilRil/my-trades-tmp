import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { MarketService } from '../market/market.service';
import { IntTrade } from '../trade/trade.service';

@Component({
    selector: 'price-input-dialog',
    templateUrl: './priceInputDialog.component.html'
})
export class PriceInputDialogComponent {
    isPartial: boolean = false;

    constructor(
        private marketService: MarketService,
        public dialog: MatDialog,
        public dialogRef: MatDialogRef<PriceInputDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: { trade: IntTrade; quantitySold: number; priceSold: number; amountAvailable: number }
    ) {}

    cancel(): void {
        this.dialog.closeAll();
    }

    sell(): void {
        this.dialogRef.close({quantitySold: this.data.quantitySold, priceSold: this.data.priceSold});
    }

    getCurrencySymbol(symbol): string {
        return this.marketService.getCurrencyIcon(symbol);
    }
}
