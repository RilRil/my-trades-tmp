import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IntTradePosition} from '../trade/trade.service';
import {MarketService} from '../market/market.service';
import {MatDialog} from '@angular/material';
import {PositionInputDialogComponent} from '../positionInputDialog/positionInputDialog.component';

@Component({
    selector: 'position-form',
    templateUrl: './positionForm.component.html',
    styleUrls: ['./positionForm.style.less']
})
export class PositionFormComponent {
    @Input() trade: {};
    @Output() submitPosition = new EventEmitter();

    constructor(private marketService: MarketService,  public dialog: MatDialog) {}

    position: IntTradePosition = {
        quantity: null,
        value: null
    };

    getCurrencySymbol(symbol): string {
        return this.marketService.getCurrencyIcon(symbol);
    }

    openInputDialog() {
        const dialogRef = this.dialog.open(PositionInputDialogComponent, {
            data: {
                trade: this.trade
            }
        });

        dialogRef.afterClosed().subscribe(data => {
            if (data) {
                this.position.quantity = data.quantity;
                this.position.value = data.value;
                this.submitPosition.emit(this.position);
            }
        });
    }
}
