import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {PositionFormComponent} from './positionForm.component';
import {SharedModule} from '../../shared.module';
import {PositionInputDialogModule} from '../positionInputDialog/positionInputDialog.module';

@NgModule({
    declarations: [PositionFormComponent],
    imports: [
        FormsModule,
        SharedModule,
        PositionInputDialogModule
    ],
    exports: [PositionFormComponent],
    providers: []
})
export class PositionFormModule {
}
