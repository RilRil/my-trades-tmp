import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CURRENCIES} from './market.constant';

@Injectable()
export class MarketService {
    constructor(private http: HttpClient) {
    }

    getLatestPrice(symbol1, symbol2, exchange) {
        //https://trade-tools-ticker.herokuapp.com/ticker/binance/TRX/BTC
        return this.http.get(`https://trade-tools-ticker.herokuapp.com/ticker/${exchange}/${symbol1}/${symbol2}`);
    }

    getCurrencyIcon(symbol) {
        return (CURRENCIES[symbol] && CURRENCIES[symbol].icon) || symbol;
    }

    round(n: number, currency: string = 'EUR'): number {
        let round = 8;
        if (CURRENCIES[currency] && CURRENCIES[currency].round) {
            round = CURRENCIES[currency].round;
        }
        return Math.round(n * Math.pow(10, round)) / Math.pow(10, round);
    }

}
