
export const CURRENCIES = {
    EUR: {
        round: 2,
        icon: '<i class="fas fa-euro-sign"></i>'
    },
    USD: {
        round: 2,
        icon: '<i class="fas fa-dollar-sign"></i>'
    },
    BTC: {
        round: 8,
        icon: '<i class="fab fa-btc"></i>'
    },
    LTC: {
        round: 8
    },
    ZRX: {
        round: 6
    }
}
