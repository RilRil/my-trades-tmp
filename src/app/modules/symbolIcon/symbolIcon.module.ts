import {NgModule} from '@angular/core';
import {SafeHtmlPipe} from '../pipes/safeHTML.pipe';
import {SymbolIconComponent} from './symbolIcon.component';
import {PipesModule} from '../pipes/pipes.module';

@NgModule({
    declarations: [SymbolIconComponent],
    exports: [SafeHtmlPipe, SymbolIconComponent],
    imports: [PipesModule]
})
export class SymbolIconModule {
}
