import {Component, OnInit, Input} from '@angular/core';
import {MarketService} from '../market/market.service';

@Component({
    selector: 'symbol-icon',
    templateUrl: './symbolIcon.component.html'
})
export class SymbolIconComponent implements OnInit {
    @Input() symbol: string;

    symbolIcon: string;

    constructor(private marketService: MarketService) {
    }

    ngOnInit(): void {
        this.symbolIcon = this.marketService.getCurrencyIcon(this.symbol);
    }
}
