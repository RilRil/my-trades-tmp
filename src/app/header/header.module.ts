import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared.module';
import {HeaderComponent} from './header.component';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [HeaderComponent],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule
    ],
    exports: [HeaderComponent]
})
export class HeaderModule {
}
