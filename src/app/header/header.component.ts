import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core'
import { AngularFireAuth } from '@angular/fire/auth'
import { auth } from 'firebase/app'
import { AuthService } from '../modules/auth/auth.service'
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore'

@Component({
    selector: 'header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.style.less']
})
export class HeaderComponent implements OnInit {
    constructor(public afAuth: AngularFireAuth, public authService: AuthService, private db: AngularFirestore) {}

    ngOnInit(): void {}

    importTrades() {
        console.log('Cyril -- import')
        let newTradesCollec = this.db.doc(`users/${this.authService.id}`).collection(`trades`)

        let tradesCollec = this.db.collection('trades', ref => ref.where('user', '==', this.authService.id))
        let trades = tradesCollec.snapshotChanges()
        trades.subscribe(t => {
            console.log('Cyril -- trades', t)
            t.map(({ payload }) => {
                let id = payload.doc.id
                let data = payload.doc.data()
                newTradesCollec.doc(id).set(data)
            })
        })
    }

    loginWithGoogle() {
        this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider())
    }
    loginWithTwitter() {
        this.afAuth.auth.signInWithPopup(new auth.TwitterAuthProvider())
    }

    logout() {
        this.afAuth.auth.signOut()
    }
}
