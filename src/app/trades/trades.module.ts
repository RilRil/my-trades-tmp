import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TradesComponent } from './trades.component';
import { TradeService } from '../modules/trade/trade.service';
import { MarketService } from '../modules/market/market.service';
import { TradeModule } from '../modules/trade/trade.module';
import { SharedModule } from '../shared.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AuthGuardService } from '../modules/guards/authGuard.service';

let routes = [{ path: '', component: TradesComponent, canActivate: [AuthGuardService] }];
@NgModule({
    imports: [CommonModule, FormsModule, TradeModule, SharedModule, RouterModule.forChild(routes)],
    providers: [TradeService, MarketService],
    declarations: [TradesComponent]
})
export class TradesModule {}
