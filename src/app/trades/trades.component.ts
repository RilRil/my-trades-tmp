import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IntTrade, TradeService } from '../modules/trade/trade.service';

@Component({
    selector: 'app-trades',
    templateUrl: './trades.component.html',
    styleUrls: ['./trades.component.less']
})
export class TradesComponent implements OnInit {
    private emptyTrade: IntTrade = {
        exchange: 'binance',
        user: null,
        symbol1: '',
        symbol2: '',
        positions: [],
        canceled: false,
        open: true,
        dateClose: null,
        dateOpen: null,
        priceClose: null
    };
    newTrade: IntTrade = this.emptyTrade;
    trades: Observable<IntTrade[]>;

    constructor(private tradeService: TradeService) {
        this.trades = this.tradeService.getTrades();
    }

    ngOnInit() {}

    addNewTrade() {
        if (this.newTrade.symbol1 !== '' && this.newTrade.symbol2 !== '') {
            this.tradeService.saveNewTrade(this.newTrade).then(ref => {
                this.cleanNewTrade();
            });
        }
    }

    cleanNewTrade() {
        this.newTrade = this.emptyTrade;
    }
}
