import {NgModule} from '@angular/core';
import {MatCheckboxModule, MatButtonModule, MatProgressBarModule, MatCardModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatMenuModule, MatIconModule, MatOptionModule, MatSelectModule} from '@angular/material';
import {PipesModule} from './modules/pipes/pipes.module';
import {SymbolIconModule} from './modules/symbolIcon/symbolIcon.module';

@NgModule({
    imports: [
        MatButtonModule,
        MatProgressBarModule,
        MatCardModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatMenuModule,
        MatIconModule,
        MatSelectModule,
        MatOptionModule,
        MatCheckboxModule,
        PipesModule,
        SymbolIconModule
    ],
    exports: [
        MatCheckboxModule,
        MatButtonModule,
        MatProgressBarModule,
        MatCardModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatMenuModule,
        MatIconModule,
        MatSelectModule,
        MatOptionModule,
        PipesModule,
        SymbolIconModule
    ]
})
export class SharedModule {
}
