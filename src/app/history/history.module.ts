import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { HistoryComponent } from './history.component'
import { RouterModule } from '@angular/router'
import { AuthGuardService } from '../modules/guards/authGuard.service'
import { TradeService } from '../modules/trade/trade.service'
import { MarketService } from '../modules/market/market.service'
import { SharedModule } from '../shared.module'
import { StatisticsModule } from '../modules/statistics/statistics.module'
import { StatisticsService } from '../modules/statistics/statistics.service'

let routes = [{ path: '', component: HistoryComponent, canActivate: [AuthGuardService] }]

@NgModule({
    imports: [CommonModule, SharedModule, RouterModule.forChild(routes), StatisticsModule],
    providers: [TradeService, MarketService, StatisticsService],
    declarations: [HistoryComponent]
})
export class HistoryModule {}
