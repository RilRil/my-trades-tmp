import { Component } from '@angular/core'
import { TradeService, IntTrade } from '../modules/trade/trade.service'
import * as moment from 'moment'
import { MarketService } from '../modules/market/market.service'
import { StatisticsService } from '../modules/statistics/statistics.service'
import { Observable } from 'rxjs'
import * as _ from 'underscore'

@Component({
    selector: 'app-history',
    templateUrl: './history.component.html',
    styleUrls: ['./history.component.less']
})
export class HistoryComponent {
    trades: any[]
    ratiosWinLoss: any[]
    statistics = {}
    nbTrades: number = 0
    nbTradesWinner: number = 0
    nbTradesLoser: number = 0
    ratioWinner: number = 1
    profitLoss = []
    profitLossMap = {}
    averageProfitLossP: number = 0
    averageDuration: number = 0

    constructor(private tradeService: TradeService, private marketService: MarketService, private statsService: StatisticsService) {

        this.tradeService.getHistoryTrades().subscribe(trades => {
            this.trades = trades.map(t => {
                let trade = t as IntTrade
                let data = this.tradeService.getTradeData(<IntTrade>trade)
                // console.log('cyril -- trade', trade, data)

                let dateOpenStr = moment(trade.dateOpen).format('DD/MM/YYYY HH:mm:ss')
                let dateCloseStr = moment(trade.dateClose).format('DD/MM/YYYY HH:mm:ss')
                let duration = Math.round(((trade.dateClose - trade.dateOpen) / 1000 / 3600) * 100) / 100
                this.averageDuration += duration
                let winner = trade.priceClose - data.averagePrice >= 0
                this.nbTradesWinner += winner ? 1 : 0
                this.nbTradesLoser += winner ? 0 : 1

                let pl = tradeService.getProfit(trade.priceClose, data.averagePrice, data.amount, trade.symbol2)
                let plP = tradeService.getPercentageProfit(trade.priceClose, data.averagePrice)

                this.averageProfitLossP += plP

                if (this.profitLossMap[trade.symbol2]) {
                    this.profitLossMap[trade.symbol2].pl += pl
                    this.profitLossMap[trade.symbol2].invest += data.value
                } else {
                    this.profitLossMap[trade.symbol2] = { pl, invest: data.value }
                }

                let obj = { pl, plP, value: data.value, duration, winner, data, dateOpenStr, dateCloseStr, ...trade }
                return obj
            })
            this.nbTrades = this.trades.length
            this.ratioWinner = marketService.round(this.nbTradesWinner / this.nbTradesLoser)
            this.averageDuration = marketService.round(this.averageDuration / this.nbTrades)
            this.averageProfitLossP = marketService.round(this.averageProfitLossP / this.nbTrades)
            this.profitLoss = Object.entries(this.profitLossMap).map(([symbol, obj]: [string, { pl; invest }]) => ({
                brut: marketService.round(obj.pl as number, symbol),
                pct: marketService.round((obj.pl / obj.invest) * 100),
                invest: obj.invest,
                symbol
            }))
        })
    }
}
