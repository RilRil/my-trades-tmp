import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { HeaderModule } from './header/header.module';
import { SharedModule } from './shared.module';
import { AuthGuardService } from './modules/guards/authGuard.service';
import { AuthService } from './modules/auth/auth.service';
import { LoginModule } from './login/login.module';
import { LoginComponent } from './login/login.component';

const appRoutes: Routes = [
    { path: '', redirectTo: 'trades', pathMatch: 'full' },
    { path: 'trades', loadChildren: './trades/trades.module#TradesModule', canActivate: [AuthGuardService] },
    { path: 'history', loadChildren: './history/history.module#HistoryModule', canActivate: [AuthGuardService] },
    { path: 'login', component: LoginComponent }
];

@NgModule({
    declarations: [AppComponent],
    imports: [
        FormsModule,
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule.forRoot(appRoutes),
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        HttpClientModule,
        HeaderModule,
        SharedModule,
        LoginModule
    ],
    providers: [AngularFirestore, AuthGuardService, AuthService],
    bootstrap: [AppComponent]
})
export class AppModule {}
